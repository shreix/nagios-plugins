#!/bin/bash

#############################################################
# Simple Nagios plugin to check free memory percentage on   #
# the local machine.										#
# Author: Shrikant Patnaik (http:://shrikantpatnaik.com)    #
#############################################################

VERSION="Version 1.0"
AUTHOR="(c) Shrikant Patnaik (me@shrikantpatnaik.com)"

PROGNAME=`/usr/bin/basename $0`

STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

print_version () {
	echo "$PROGNAME - $VERSION"
}

print_usage () {
	echo "Usage: $PROGNAME [-v] -w <limit in percent> -c <limit in percent> [-i][include cached] [-b][include bufferred]"
}

print_usage_and_exit () {
	print_usage
	exit $1
}

print_help () {
	print_version
	echo "$AUTHOR\n\nCheck free memomry percentage on local machine\n"
	print_usage
}

get_mem () {
	cat /proc/meminfo | grep "^$1" | sed -e "s/$1://g" | sed -e 's/ //g' | sed -e 's/kB//g';
}

while getopts “hibw:c:V?” opts
do
	case $opts in
		h)  print_help
			exit $STATE_OK
			;;
		w) 
			warning_level=$OPTARG				
			;;	
		c)  
			critical_level=$OPTARG	
			;;
		b)
			include_buffer=1
			;;
		i) 
			include_cache=1
			;;
		V) 	
			print_version
			exit $STATE_OK
			;;
		-?) 
			print_usage_and_exit $STATE_OK
			;;
		:) 	
			echo "Option -$OPTARG requires an argument."
      		print_usage_and_exit $STATE_UNKNOWN
      		;;
		*) 
			echo "$PROGNAME: Invalid option '$1'"
			print_usage_and_exit $STATE_UNKNOWN
	esac
done

if [ -z "$warning_level" ]; then
	echo "Warning level must be set"
	print_usage_and_exit $STATE_UNKNOWN
elif [ -z "$critical_level" ]; then
	echo "Critical level must be set"
	print_usage_and_exit $STATE_UNKNOWN
fi	 

free=`get_mem MemFree`
total=`get_mem MemTotal`

if [ -z "$include_cache" ]; then
	cached=`get_mem Cached`
	free=$(($free+$cached))
fi

if [ -z "$include_buffer" ]; then
	buffered=`get_mem Buffers`
	free=$(($free+$buffered))
fi

used=$(echo "($free / $total)*100" | bc -l)
used=${used:0:5}

if [ $(echo "$used > $critical_level" | bc -l) -eq "1" ]; then
	echo "MEMORY CRITICAL - $used% is used out of $total kB"
	exit $STATE_CRITICAL
elif [ $(echo "$used > $warning_level" | bc -l) -eq "1" ]; then
	echo "MEMORY WARNING - $used% is used out of $total kB"
	exit $STATE_WARNING
else
	echo "MEMORY OK - $used% is used out of $total kB"
	exit $STATE_OK
fi
